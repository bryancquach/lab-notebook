{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "# Pre-eQTL analysis control samples data wrangling\n",
    "\n",
    "__Author:__ Bryan Quach  \n",
    "__Date:__ April 24, 2017\n",
    "\n",
    "## Objective: Create phenotype and design matrices for eQTL analysis\n",
    "\n",
    "The eQTL software requires as input a [design matrix](https://en.wikipedia.org/wiki/Design_matrix) and a phenotype matrix. This document outlines the steps we take in generating the matrices for downstream tissue-specific eQTL analyses in control samples.\n",
    "\n",
    "### Estimated Analysis Time: <1 days\n",
    "\n",
    "## Analysis Outline\n",
    "\n",
    "* Creating tissue-specific phenotype matrices\n",
    "* Creating tissue-specific covariate matrices\n",
    "\n",
    "## Phenotype matrix construction\n",
    "\n",
    "The phenotype we have is gene expression values in liver, lung, and kidney tissue. We start with RSEM derived TPM count matrices. In a previous notebook, we applied between-sample normalization to generate our eQTL phenotype matrices. In this notebook, we take the strategy of using TPM (within-sample normalization). Further thought may be needed to deteremine which is more sensible. Within-sample normalization seems more fitting for setting an \"across-gene\" threshold for filtering \"lowly expressed\" genes since it accounts for gene length. Alternatively, between-sample normalization seems more fitting for setting an \"across-sample\" criterion since the samples are on a comparable scale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "library(DESeq2)\n",
    "\n",
    "#Load TPM data\n",
    "load(\"data/tpm_data.original.Rdata\")\n",
    "all.cts <- cbind(txi.rsem.htsf$abundance, txi.rsem.tamu$abundance, txi.rsem.merged$abundance)\n",
    "\n",
    "#Reformat sample names\n",
    "sample.names <- sapply(strsplit(x=colnames(all.cts), split='_'), function(x){paste0(x[1:3],collapse=\"_\")})\n",
    "colnames(all.cts) <- sample.names"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "Since our initial eQTL analyses will be in control samples only, we will retain only the control sample values and seperate the TPM matrix by tissue. To reduce the number of genes we will be including in the eQTL analysis, any genes with 5% or less of samples that have >1 TPM will be removed. This cutoff value is based on a previously generated TPM distribution that showed a bimodal distribution with the lower distribution's peak near 1 TPM. The assumption we made in this plot was that the first distribution represented the lowly expressed genes, so choosing the mode of the distribution would provide a balance between sensitivity and specificity. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Separate samples by tissue type and filter out non-controls\n",
    "filtered.cts <- all.cts[,grepl(colnames(all.cts), pattern=\"_0ppm_\", ignore.case=T)]\n",
    "kidney.cts <- filtered.cts[,grepl(colnames(filtered.cts), pattern=\"kidney\", ignore.case=T)]\n",
    "liver.cts <- filtered.cts[,grepl(colnames(filtered.cts), pattern=\"liver\", ignore.case=T)]\n",
    "lung.cts <- filtered.cts[,grepl(colnames(filtered.cts), pattern=\"lung\", ignore.case=T)]\n",
    "\n",
    "#Filter out lowly expressed genes\n",
    "kidney.keep <- apply(kidney.cts, 1, function(y){length(which(y>1))}) >= 0.05*ncol(kidney.cts)\n",
    "liver.keep <- apply(liver.cts, 1, function(y){length(which(y>1))}) >= 0.05*ncol(liver.cts)\n",
    "lung.keep <- apply(lung.cts, 1, function(y){length(which(y>1))}) >= 0.05*ncol(lung.cts)\n",
    "kidney.cts <- t(kidney.cts[kidney.keep,])\n",
    "liver.cts <- t(liver.cts[liver.keep,])\n",
    "lung.cts <- t(lung.cts[lung.keep,])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "By this point the matrices of counts are almost in the correct form for use with the eQTL software. Once we have created the covariate matrices, we can convert everything into the final format. \n",
    "\n",
    "## Covariate matrix construction\n",
    "\n",
    "The covariate matrices that we construct contain the following information for each sample:\n",
    "\n",
    "* Sample ID - we use CC strain ID for this\n",
    "* Sequencing center location\n",
    "* Sequencing batch\n",
    "* Haplotype dosages for every marker"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Load sequencing center labels\n",
    "seq.center <- read.delim(\"../sequencing_center_info.txt\", header=F)\n",
    "seq.center <- cbind(seq.center, sapply(seq.center[,1], function(x){strsplit(as.character(x), split=\"_\")[[1]][3]}))\n",
    "colnames(seq.center) <- c(\"Sample\",\"Center\",\"ID\")\n",
    "\n",
    "#Load batch labels and add to partial covariate matrix\n",
    "batch <- read.delim(\"../batch_info.txt\", header=F)\n",
    "batch.indices <- sapply(seq.center$Sample, function(x){which(batch[,1]==x)})\n",
    "cov.data <- cbind(seq.center, batch[batch.indices,2])\n",
    "\n",
    "#Load mouse ID to strain map\n",
    "id.strain.map <- read.delim(\"../../id_strain_map.txt\", header=T)\n",
    "id.strain.map$ID <- as.numeric(id.strain.map$ID)\n",
    "#Add strain labels to seq.center and reorder columns\n",
    "sample.info <- cbind(cov.data, id.strain.map$Strain[sapply(seq.center$ID, function(x){which(id.strain.map$ID==x)})])\n",
    "sample.info <- sample.info[,c(1,3,5,2,4)]\n",
    "colnames(sample.info) <- c(\"Sample\", \"ID\", \"Strain\", \"Center\",\"Batch\")\n",
    "\n",
    "#Load haplotype dosage matrix\n",
    "dosages <- readRDS(\"../../genotype/20170331_covariate_matrix/results/dosage_covariate_matrix.rds\")\n",
    "\n",
    "#Create covariate matrices for each tissue\n",
    "kidney.covdata <- sample.info[sapply(rownames(kidney.cts), function(x){which(sample.info$Sample==x)}),][,c(\"Strain\",\"Center\",\"Batch\")]\n",
    "kidney.dosages <- dosages[sapply(kidney.covdata$Strain, function(x){which(rownames(dosages)==x)}),]\n",
    "kidney.covdata <- cbind(kidney.covdata, kidney.dosages)\n",
    "liver.covdata <- sample.info[sapply(rownames(liver.cts), function(x){which(sample.info$Sample==x)}),][,c(\"Strain\",\"Center\",\"Batch\")]\n",
    "liver.dosages <- dosages[sapply(liver.covdata$Strain, function(x){which(rownames(dosages)==x)}),]\n",
    "liver.covdata <- cbind(liver.covdata, liver.dosages)\n",
    "lung.covdata <- sample.info[sapply(rownames(lung.cts), function(x){which(sample.info$Sample==x)}),][,c(\"Strain\",\"Center\",\"Batch\")]\n",
    "lung.dosages <- dosages[sapply(lung.covdata$Strain, function(x){which(rownames(dosages)==x)}),]\n",
    "lung.covdata <- cbind(lung.covdata, lung.dosages)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Matrix reformatting and saving\n",
    "\n",
    "With the expression matrices and covariate matrices in the right structure, we rename the strain column to \"ID\" as required by the eQTL software, rename the rows of the expression matrices to be strain names, and save the matrices to files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>49</li>\n",
       "\t<li>15915</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 49\n",
       "\\item 15915\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 49\n",
       "2. 15915\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]    49 15915"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>49</li>\n",
       "\t<li>13681</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 49\n",
       "\\item 13681\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 49\n",
       "2. 13681\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]    49 13681"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>49</li>\n",
       "\t<li>17707</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 49\n",
       "\\item 17707\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 49\n",
       "2. 17707\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]    49 17707"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>49</li>\n",
       "\t<li>621779</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 49\n",
       "\\item 621779\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 49\n",
       "2. 621779\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]     49 621779"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>49</li>\n",
       "\t<li>621779</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 49\n",
       "\\item 621779\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 49\n",
       "2. 621779\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]     49 621779"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>49</li>\n",
       "\t<li>621779</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 49\n",
       "\\item 621779\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 49\n",
       "2. 621779\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]     49 621779"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "colnames(kidney.covdata)[1] <- \"ID\"\n",
    "colnames(liver.covdata)[1] <- \"ID\"\n",
    "colnames(lung.covdata)[1] <- \"ID\"\n",
    "rownames(kidney.cts) <- sample.info[sapply(rownames(kidney.cts), function(x){which(sample.info$Sample==x)}),][,\"Strain\"]\n",
    "rownames(liver.cts) <- sample.info[sapply(rownames(liver.cts), function(x){which(sample.info$Sample==x)}),][,\"Strain\"]\n",
    "rownames(lung.cts) <- sample.info[sapply(rownames(lung.cts), function(x){which(sample.info$Sample==x)}),][,\"Strain\"]\n",
    "\n",
    "#Matrix size sanity check - each should be strain x gene/covariate\n",
    "#Covariate matrices should be equal width\n",
    "dim(kidney.cts)\n",
    "dim(liver.cts)\n",
    "dim(lung.cts)\n",
    "dim(kidney.covdata)\n",
    "dim(liver.covdata)\n",
    "dim(lung.covdata)\n",
    "\n",
    "#Save matrices\n",
    "saveRDS(object=kidney.cts, file=\"results/expmat_control_kidney.rds\")\n",
    "saveRDS(object=liver.cts, file=\"results/expmat_control_liver.rds\")\n",
    "saveRDS(object=lung.cts, file=\"results/expmat_control_lung.rds\")\n",
    "saveRDS(object=kidney.covdata, file=\"results/covdata_control_kidney.rds\")\n",
    "saveRDS(object=liver.covdata, file=\"results/covdata_control_liver.rds\")\n",
    "saveRDS(object=lung.covdata, file=\"results/covdata_control_lung.rds\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.4.0"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "171px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
