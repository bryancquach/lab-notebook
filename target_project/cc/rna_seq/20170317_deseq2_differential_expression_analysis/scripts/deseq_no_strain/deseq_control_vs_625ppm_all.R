library(DESeq2)
library(BiocParallel)
register(MulticoreParam(6)) #Enable six cores for parallel processing
load("deseq_datasets.Rdata")
dds.subset.all <- subset(dds.all, dds.all$condition!="1500ppm")
dds.subset.all <- dds.subset.all[rowSums(counts(dds.subset.all))>10,]
dds.subset.all$condition <- relevel(dds.subset.all$condition, ref="0ppm")
deseq.all1 <- DESeq(dds.subset.all, parallel=T)
results.all1 = results(deseq.all1, parallel=T)
save(file="rna_seq_control_vs_625ppm_all.Rdata", list=c("deseq.all1", "results.all1")
