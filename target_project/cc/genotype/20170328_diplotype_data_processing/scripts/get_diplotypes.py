# Retrieve diplotype probability data from CC resource webpage

import requests

fin = open("data/cc_diplotype_links.txt", 'r') #file containing links to downloadable files 
for line in fin:
  r = requests.get(line)
  filename = "_".join(r.headers["Content-Disposition"].split(';')[1].split('=')[1].split('/'))
  fout = open("data/" + filename, 'w')
  fout.write(r.text)
  fout.close()
fin.close()
