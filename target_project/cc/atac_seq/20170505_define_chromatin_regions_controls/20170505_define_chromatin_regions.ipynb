{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "# Defining TaRGET Project CC ATAC-seq Data Chromatin Regions\n",
    "\n",
    "__Author:__ Bryan Quach  \n",
    "__Date:__ May 5, 2017\n",
    "\n",
    "## Objective\n",
    "\n",
    "### Estimated Analysis Time: <1 days*\n",
    "\n",
    "To compare chromatin accessibility profiles across samples, we define a common set of chromatin regions across samples. Using these regions, we then quantify the level of chromatin accessibility per sample at each of the regions using ATAC-seq read alignments. These will be used in downstream chromatin QTL (cQTL) analysis. This notebook will focus on generating these chromatin regions per tissue using only the CC mice control samples.\n",
    "\n",
    "**This estimate optimistically assumes that the analysis will be done in a high performance computing environment where tasks can be distributed and with all necessary software already installed. Limitations computing power can significantly increase analysis time.*\n",
    "\n",
    "## Required software\n",
    "\n",
    "* [BEDTools](http://bedtools.readthedocs.io)\n",
    "* [GenomicRanges (R package)](https://bioconductor.org/packages/release/bioc/html/GenomicRanges.html)\n",
    "* [IRanges (R package)](https://bioconductor.org/packages/release/bioc/html/IRanges.html)\n",
    "* [DESeq2 (R package)](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)\n",
    "* [XVector (R package)](https://bioconductor.org/packages/release/bioc/html/XVector.html)\n",
    "* [dplyr (R package)](https://cran.r-project.org/web/packages/dplyr/index.html)\n",
    "\n",
    "## The Data\n",
    "\n",
    "The data we are working with are 50 base pair paired-end sequencing reads that have been aligned to CC strain specific genomes then converted to mm9 (C57BL6/J) reference genome coordinate space. From these data, we also have derived open chromatin peak calls in [narrow peak format (NPF)](https://genome.ucsc.edu/FAQ/FAQformat.html#format12) using [F-seq](fureylab.web.unc.edu/software/fseq/) and sorted and shifted BED files. Both of these derived datasets will be used in the analysis. We will be doing analyses that remove the original sequencing results where re-sequencing results are available, so not all the data will be processed. For samples that have been re-sequenced, we will skip processing of the original sequencing data. The data is processed on the UNC Longleaf computing cluster which uses SLURM as its job handling system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Set working directory\n",
    "cd /proj/fureylab/data/ATAC/mouse/CC\n",
    "\n",
    "#Load modules\n",
    "module load bedtools\n",
    "\n",
    "#Perform file clean-up as a safety pre-caution\n",
    "rm CC0*/*/*top50k.npf\n",
    "rm CC0*/*/*300bpNS.bed\n",
    "rm CC0*/*/*csaw*bam.bai\n",
    "rm CC0*/*/*csaw*bam\n",
    "\n",
    "#For each control sample npf file, retrieve the top 50k open chromatin peaks:\n",
    "for file in CC0*/*Control*/*npf\n",
    "do\n",
    "    echo $file\n",
    "    sbatch -n 1 --mem=1G --time=1:00:00 --wrap=\"sort -rnk 7 $file | head -n 50000 > ${file/.npf/.top50k.npf}\"\n",
    "done\n",
    "\n",
    "#Check that each new npf file has 50k peaks\n",
    "for file in CC0*/*Control*/*top50k.npf; do wc -l $file; done\n",
    "\n",
    "#Create tissue specific peak union sets\n",
    "cat ./CC*/*Control*/*KIDNEY*top50k.npf > top50k_cat_kidney.npf\n",
    "cat ./CC*/*Control*/*LUNG*top50k.npf > top50k_cat_lung.npf\n",
    "cat ./CC*/*Control*/*LIVER*top50k.npf > top50k_cat_liver.npf\n",
    "sbatch -n 1 --mem=6G --time=1:00:00 --wrap='sort -k1,1 -k2,2n top50k_cat_kidney.npf > top50_cat_kidney_sorted.bed'\n",
    "sbatch -n 1 --mem=6G --time=1:00:00 --wrap='sort -k1,1 -k2,2n top50k_cat_lung.npf > top50_cat_lung_sorted.bed'\n",
    "sbatch -n 1 --mem=6G --time=1:00:00 --wrap='sort -k1,1 -k2,2n top50k_cat_liver.npf > top50_cat_liver_sorted.bed'\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap='bedtools merge -i top50_cat_kidney_sorted.bed -c 1 -o count > top50k_kidney_union.bed'\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap='bedtools merge -i top50_cat_lung_sorted.bed -c 1 -o count > top50k_lung_union.bed'\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap='bedtools merge -i top50_cat_liver_sorted.bed -c 1 -o count > top50k_liver_union.bed'\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap='sort -k1,1 -k2,2n top50k_kidney_union.bed > top50k_kidney_union.sorted.bed'\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap='sort -k1,1 -k2,2n top50k_liver_union.bed > top50k_liver_union.sorted.bed'\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap='sort -k1,1 -k2,2n top50k_lung_union.bed > top50k_lung_union.sorted.bed'\n",
    "\n",
    "#Create windows\n",
    "sbatch -n 8 -N 1-1 --mem=48G --time=36:00:00 -o create_windows_kidney.out --wrap='Rscript /proj/fureylab/code_repository/bquach/create_windows_parallel.R top50k_kidney_union.sorted.bed 300bp_windows_kidney.bed 8'\n",
    "sbatch -n 8 -N 1-1 --mem=48G --time=36:00:00 -o create_windows_liver.out --wrap='Rscript /proj/fureylab/code_repository/bquach/create_windows_parallel.R top50k_liver_union.sorted.bed 300bp_windows_liver.bed 8'\n",
    "sbatch -n 8 -N 1-1 --mem=48G --time=36:00:00 -o create_windows_lung.out --wrap='Rscript /proj/fureylab/code_repository/bquach/create_windows_parallel.R top50k_lung_union.sorted.bed 300bp_windows_lung.bed 8'\n",
    "sbatch -n 1 -N 1-1 --mem=8G --time=2:00:00 --wrap=\"bedtools sort -i 300bp_windows_kidney.bed > 300bp_windows_kidney.sorted.bed\"\n",
    "sbatch -n 1 -N 1-1 --mem=8G --time=2:00:00 --wrap=\"bedtools sort -i 300bp_windows_liver.bed > 300bp_windows_liver.sorted.bed\"\n",
    "sbatch -n 1 -N 1-1 --mem=8G --time=2:00:00 --wrap=\"bedtools sort -i 300bp_windows_lung.bed > 300bp_windows_lung.sorted.bed\"\n",
    "\n",
    "#Calculate coverage in windows for each shifted sorted bed file\n",
    "#BEDTools version must be at least 2.24.0\n",
    "for file in CC*/*Control_KIDNEY*/ss*[0-9].bed\n",
    "do\n",
    "    sbatch -n 1 --mem=12G --time=1:00:00 -o $(basename ${file/.bed/.out}) --wrap=\"bedtools coverage -sorted -counts -b $file -a 300bp_windows_kidney.sorted.bed > ${file/.bed/.coverage_300bp_kidney.bed}\"\n",
    "done\n",
    "\n",
    "for file in CC*/*Control_LIVER*/ss*[0-9].bed\n",
    "do\n",
    "    sbatch -n 1 --mem=12G --time=1:00:00 -o $(basename ${file/.bed/.out}) --wrap=\"bedtools coverage -sorted -counts -b $file -a 300bp_windows_liver.sorted.bed > ${file/.bed/.coverage_300bp_liver.bed}\"\n",
    "done\n",
    "\n",
    "for file in CC*/*Control_LUNG*/ss*[0-9].bed\n",
    "do\n",
    "    sbatch -n 1 --mem=12G --time=1:00:00 -o $(basename ${file/.bed/.out}) --wrap=\"bedtools coverage -sorted -counts -b $file -a 300bp_windows_lung.sorted.bed > ${file/.bed/.coverage_300bp_lung.bed}\"\n",
    "done\n",
    "\n",
    "#Get coverage data column and merge\n",
    "kidney_cov_files=($(ls CC*/*/ss*.coverage_300bp_kidney.bed))\n",
    "liver_cov_files=($(ls CC*/*/ss*.coverage_300bp_liver.bed))\n",
    "lung_cov_files=($(ls CC*/*/ss*.coverage_300bp_lung.bed))\n",
    "for i in ${kidney_cov_files[*]}; do cut -f4,4 $i > $(basename ${i/.bed/.txt}); done\n",
    "for i in ${liver_cov_files[*]}; do cut -f4,4 $i > $(basename ${i/.bed/.txt}); done\n",
    "for i in ${lung_cov_files[*]}; do cut -f4,4 $i > $(basename ${i/.bed/.txt}); done\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap=\"paste ss*.coverage_300bp_kidney.txt > count_matrix_kidney.txt\"\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap=\"paste ss*.coverage_300bp_liver.txt > count_matrix_liver.txt\"\n",
    "sbatch -n 1 --mem=8G --time=1:00:00 --wrap=\"paste ss*.coverage_300bp_lung.txt > count_matrix_lung.txt\"\n",
    "\n",
    "#Combine all count matrix data\n",
    "awk '{OFS=\".\"} {print $1,$2,$3}' ${kidney_cov_files[0]} > count_matrix_kidney_rownames.txt\n",
    "awk '{OFS=\".\"} {print $1,$2,$3}' ${liver_cov_files[0]} > count_matrix_liver_rownames.txt\n",
    "awk '{OFS=\".\"} {print $1,$2,$3}' ${lung_cov_files[0]} > count_matrix_lung_rownames.txt\n",
    "ls ss*.coverage_300bp_kidney.txt | cut -f1,1 -d'.' | awk '{print substr($0, 3)}' > count_matrix_kidney_colnames.txt\n",
    "ls ss*.coverage_300bp_liver.txt | cut -f1,1 -d'.' | awk '{print substr($0, 3)}' > count_matrix_liver_colnames.txt\n",
    "ls ss*.coverage_300bp_lung.txt | cut -f1,1 -d'.' | awk '{print substr($0, 3)}' > count_matrix_lung_colnames.txt\n",
    "cat <(echo \"region\") count_matrix_kidney_colnames.txt > count_matrix_kidney_colnames.txt.tmp\n",
    "cat <(echo \"region\") count_matrix_liver_colnames.txt > count_matrix_liver_colnames.txt.tmp\n",
    "cat <(echo \"region\") count_matrix_lung_colnames.txt > count_matrix_lung_colnames.txt.tmp\n",
    "mv count_matrix_kidney_colnames.txt.tmp count_matrix_kidney_colnames.txt\n",
    "mv count_matrix_liver_colnames.txt.tmp count_matrix_liver_colnames.txt\n",
    "mv count_matrix_lung_colnames.txt.tmp count_matrix_lung_colnames.txt\n",
    "perl -pi -e \"s/\\n/\\t/g\" count_matrix_kidney_colnames.txt\n",
    "perl -pi -e \"s/\\n/\\t/g\" count_matrix_liver_colnames.txt\n",
    "perl -pi -e \"s/\\n/\\t/g\" count_matrix_lung_colnames.txt\n",
    "perl -pi -e \"s/\\t$//g\" count_matrix_kidney_colnames.txt #remove tag at end of line\n",
    "perl -pi -e \"s/\\t$//g\" count_matrix_liver_colnames.txt #remove tag at end of line\n",
    "perl -pi -e \"s/\\t$//g\" count_matrix_lung_colnames.txt #remove tag at end of line\n",
    "cat count_matrix_kidney_colnames.txt <(echo \"\") <(paste count_matrix_kidney_rownames.txt count_matrix_kidney.txt) > atac_regions_count_matrix_kidney.txt\n",
    "cat count_matrix_liver_colnames.txt <(echo \"\") <(paste count_matrix_liver_rownames.txt count_matrix_liver.txt) > atac_regions_count_matrix_liver.txt\n",
    "cat count_matrix_lung_colnames.txt <(echo \"\") <(paste count_matrix_lung_rownames.txt count_matrix_lung.txt) > atac_regions_count_matrix_lung.txt\n",
    "\n",
    "#Cleanup\n",
    "#rm count_matrix*txt 300bp*bed top50*bed CC*/*Control*/*coverage_300bp_*bed *out ss*txt top*npf"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "117px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
