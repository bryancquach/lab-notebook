# Furey Lab Notebook

__Author__: Bryan Quach

This repository serves to log the analyses that I do for Furey Lab projects. The analyses are all stored as [jupyter notebooks](https://jupyter.org/index.html) and lossely organized into a directory structure by project. Some analyses are contained within a standalone notebook file, while others will have a whole subdirectory structure contains data, scripts, results, etc. All the notebooks follow a date convention of `yyymmdd` followed by some general descriptor of the analysis.